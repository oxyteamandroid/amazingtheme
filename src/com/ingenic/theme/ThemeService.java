/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingTheme Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.theme;

import java.io.File;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;
import com.ingenic.theme.utils.ThemeUtils;
import com.ingenic.theme.utils.UnzipUtils;

/**
 * 主题服务，处理与手机端进行主题切换的逻辑
 *
 * @author ShiGuanghua
 *
 */
public class ThemeService extends Service implements
        FileTransactionModelCallback {
    /**
     * 文件传输所需UUID
     */
    private static final String FILE_UUID = "a1dc19e2-17a4-0797-9362-68a0dd4bfb68";

    /**
     * 文件传输模型
     */
    private FileTransactionModel mFileTransactionModel;

    /**
     * 主题发生改变的action
     */
    private final String THEME_CHANGE = "com.ingenic.theme.change";

    /**
     * 主题切换完毕action
     */
    private static final String THEME_CHANGE_OVER = "com.ingenic.theme.change.over";

    /**
     * 数据传输UUID
     */
    private static final  String DATA_UUID = "635218be-c63a-11e4-af0f-000b2f579610";
    private DataTransactor mTransactor;

    /**
     * 通道是否可用
     */
    private boolean mDataAvailable;

    /**
     * 文件传输是否可用
     */
    private boolean mFileAvaiable;

    /**
     * 文件接受进度
     */
    private int mProgress = 100;

    private String mFileName = null;

    /**
     * 接收的文件，在手表中的路径
     */
    private final String ABSPATH = "/storage/emulated/0/iwds/";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9999, notification);

        IntentFilter mThemeFilter = new IntentFilter();
        mThemeFilter.addAction(THEME_CHANGE_OVER);
        this.registerReceiver(mChangeThemeRecevier, mThemeFilter);

        if (mFileTransactionModel == null)
            mFileTransactionModel = new FileTransactionModel(
                    getApplicationContext(), this, FILE_UUID);
        if (mTransactor == null)
            mTransactor = new DataTransactor(getApplicationContext(),
                    new DataTransfer(), DATA_UUID);
        if (!mFileAvaiable) {
            mFileTransactionModel.start();
        }
        if (!mDataAvailable) {
            mTransactor.start();
        }

    }

    private BroadcastReceiver mChangeThemeRecevier = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (THEME_CHANGE_OVER.equals(action)) {
                // 操作完毕后答复手机端
                if (mDataAvailable && mTransactor != null) {
                    mTransactor.send("finish theme change");
                }
            }

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFileTransactionModel != null) {
            mFileTransactionModel.stop();
        }
        if (mTransactor != null) {
            mTransactor.stop();
        }
        this.unregisterReceiver(mChangeThemeRecevier);
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (isConnected) {
        } else {
        }
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (isAvailable) {
            mFileAvaiable = true;
        } else {
            mFileAvaiable = false;
            // 文件传输由于通道不可用而失败，因此删除残余文件
            if (mFileName != null && mProgress != 100) {
                File zipFile = new File(ABSPATH + mFileName);
                if (zipFile.exists()) {
                    zipFile.delete();
                    mFileName = null;
                    mProgress = 100;
                    zipFile = null;
                }
            }
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        if (result.getResultCode() == DataTransactResult.RESULT_OK) {
        } else {
        }
    }

    @Override
    public void onRequestSendFile(FileInfo info) {
        mFileName = info.name;
        // 默认都是接收文件
        mFileTransactionModel.notifyConfirmForReceiveFile();
    }

    @Override
    public void onSendFileProgress(int progress) {

    }

    @Override
    public void onRecvFileProgress(int progress) {
        mProgress = progress;
    }

    @Override
    public void onConfirmForReceiveFile() {

    }

    @Override
    public void onCancelForReceiveFile() {

    }

    @Override
    public void onFileArrived(final File file) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    // 解压文件
                    UnzipUtils.upZipFile(file, ThemeUtils.WHICH_THEME_PATH
                            + File.separator);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String mFileName = file.getName();
                String fileNames = "."
                        + String.format(
                                "%03d",
                                Integer.parseInt(mFileName.substring(0,
                                        mFileName.lastIndexOf(".zip"))));
                // 修改XML文件
                changeThemeXml(fileNames);
                // 发送广播
                sendBroadcastForTheme();

                // 删除文件
                file.delete();

            }
        }).start();

    }

    private void sendBroadcastForTheme() {
        Intent intent = new Intent(THEME_CHANGE);
        sendBroadcast(intent);
    }

    private void changeThemeXml(String mFileName) {
        if (ThemeUtils.writeThemes(ThemeUtils.WHICH_THEME_PATH,
                ThemeUtils.WHICH_THEME_NAME, ThemeUtils.WHICH_THEME_PATH
                        + File.separator + mFileName, ThemeUtils.DEFAULT_NAME)) {
            ThemeUtils.formatXMLFile(ThemeUtils.WHICH_THEME_PATH
                    + File.separator + ThemeUtils.WHICH_THEME_NAME);
        } else {
        }
    }

    private class DataTransfer implements DataTransactorCallback {

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            if (isConnected) {
            } else {
            }
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            if (isAvailable) {
                mDataAvailable = true;
            } else {
                mDataAvailable = false;
            }
        }

        @Override
        public void onSendResult(DataTransactResult result) {
            if (result.getResultCode() == DataTransactResult.RESULT_OK) {
            } else {
            }
        }

        @Override
        public void onDataArrived(Object object) {

        }

        @Override
        public void onSendFileProgress(int progress) {

        }

        @Override
        public void onRecvFileProgress(int progress) {

        }

        @Override
        public void onRecvFileInterrupted(int arg0) {

        }

        @Override
        public void onSendFileInterrupted(int arg0) {

        }

    }

    @Override
    public void onFileTransferError(int arg0) {

    }

    @Override
    public void onRecvFileInterrupted(int arg0) {

    }

    @Override
    public void onSendFileInterrupted(int arg0) {

    }

}
