/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *   
 *  Elf/AmazingTheme Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.theme.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Xml;

/**
 * 主题工具类
 *
 * @author ShiGuanghua
 *
 */

public class ThemeUtils {

    public static final String ns = null;

    public static final String THEME_FILE_PATH = "/system/themes/local_theme/";
    public static final String THEME_PREVIEW_PATH = "/system/themes/local_preview";
    public static final String THEME_PREVIEW_NAME = "theme-name";
    public static final String THEME_PIC_NAME = "theme_preview.png";
    public static String WHICH_THEME_PATH = "mnt/sdcard/.amazingtheme";
    public static final String WHICH_THEME_NAME = "choose_theme.xml";
    private final static String DEFAULT_PATH = "/system/theme/default_theme";
    public final static String DEFAULT_NAME = "theme.xml";
    public static String THEME_TAG_NAME = "theme";
    public static String PREVIEW_TAG_NAME = "theme_preview";
    public static String PREVIEW_TEXT_NAME = "preview_info";
    public static String WHICH_THEME = "whictheme";
    public static final String CHANGE_THEME_OVER = "cn.ingenic.theme.changeover";
    public static final String CHANGE_THEME_ING = "cn.ingenic.theme.change";
    private static Bitmap mTempDrawable = null;
    private static ArrayList<HashMap<String, String>> mListMap = null;
    private static Bitmap tempBitmap = null;

    /**
     * 保存主题临时路径
     */
    public static String themeTempPath = null;

    private static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
            WHICH_THEME_PATH = sdDir + "/.amazingtheme";
        }
        return WHICH_THEME_PATH;
    }

    /**
     * @param: drawPath 图片的位置地址
     * @return: 返回 Bitmap
     */
    public static Bitmap getDrawable(String drawPath) {
        mTempDrawable = null;
        try {
            File fileTemp = new File(themeTempPath + File.separator + drawPath);
            if (fileTemp.exists()) {
                FileInputStream input = new FileInputStream(fileTemp);
                Bitmap bmp = BitmapFactory
                        .decodeStream(new BufferedInputStream(input));
                mTempDrawable = bmp;
                input.close();
            } else {
                return mTempDrawable;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mTempDrawable != null)
            return mTempDrawable;
        else
            return null;
    }

    public static void releaseBitMap() {
        if (tempBitmap != null) {
            tempBitmap.recycle();
            tempBitmap = null;
        }
    }

    /**
     * the pic of theme
     *
     * @param preTag
     * @param secondTag
     * @return
     */
    public static synchronized Bitmap getThemeDrawable(String preTag,
            String secondTag) {
        String tempDraPath = null;
        mListMap = ThemeUtils.getThemeValues(preTag, secondTag);
        if (mListMap != null && mListMap.size() > 1) {
            tempDraPath = mListMap.get(1).get("image");
        } else {
            tempDraPath = null;
        }
        tempBitmap = null;
        if (tempDraPath != null) {
            tempBitmap = ThemeUtils.getDrawable(tempDraPath);
            return tempBitmap;
        } else {
            return null;
        }
    }

    /**
     * @param: mListMap HashMap 的 List 列表 （其中包含，中文，英文，繁体的标题信息）
     * @return: 返回当前本地的标题信息（本地为中文，返回中文，本地为英文，返回英文，本地为繁体，返回繁体, 其它，返回英文）
     */
    public static String getTitleName(
            ArrayList<HashMap<String, String>> mListMap) {
        String language = Locale.getDefault().toString();
        String titleName;
        if (language.equals("zh_CN")) {
            titleName = mListMap.get(2).get("name-zhCN");
        } else if (language.equals("zh_TW")) {
            titleName = mListMap.get(3).get("name-zhTW");
        } else if (language.equals("in_ID")) {
            titleName = mListMap.get(6).get("name-in_ID");
        } else if (language.equals("ru_RU")) {
            titleName = mListMap.get(7).get("name-ru_RU");
        } else if (language.equals("ar_EG")) {
            titleName = mListMap.get(8).get("name-ar_EG");
        } else if (language.equals("ko_KR")) {
            titleName = mListMap.get(9).get("name-ko_KR");
        } else if (language.equals("el_GR")) {
            titleName = mListMap.get(10).get("name-el_GR");
        } else if (language.equals("fr_FR")) {
            titleName = mListMap.get(11).get("name-fr_FR");
        } else if (language.equals("de_DE")) {
            titleName = mListMap.get(12).get("name-de_DE");
        } else if (language.equals("es_ES")) {
            titleName = mListMap.get(13).get("name-es_ES");
        } else if (language.equals("it_IT")) {
            titleName = mListMap.get(14).get("name-it_IT");
        } else if (language.equals("pl_PL")) {
            titleName = mListMap.get(15).get("name-pl_PL");
        } else if (language.equals("pt_PT")) {
            titleName = mListMap.get(16).get("name-pt_PT");
        } else {
            titleName = mListMap.get(4).get("name-en_US");
        }

        return titleName;
    }

    /**
     * @param: tagPreName 一级标签 例如 launcher 就是 launcher 为一级标签
     * @param: tagName 二级标签 例如 launcher 中的 left_image_indication 就是二级标签
     * @return: 返回该二级标签下的所有信息列表，使用HashMap存放具体的一条信息
     */
    public static ArrayList<HashMap<String, String>> getThemeValues(
            String tagPreName, String tagName) {
        ArrayList<HashMap<String, String>> themeMsgList = null;
        // 读取当前的主题
        ArrayList<HashMap<String, String>> listCurrent = ThemeUtils
                .queryById(THEME_TAG_NAME, WHICH_THEME, WHICH_THEME_PATH,
                        WHICH_THEME_NAME);
        if (listCurrent != null && listCurrent.size() == 2
                && !listCurrent.get(0).get("file_path").equals("")) {
            String file_path = listCurrent.get(0).get("file_path");
            String file_name = listCurrent.get(1).get("file_name");
            themeTempPath = file_path;
            // 如果使用了系统的主题， 主题 choose_theme.xml 中 file_path 和 file_name 都置为 ""
            if (!file_path.equals("") && !file_name.equals("")) {
                themeMsgList = queryById(tagPreName, tagName, file_path,
                        file_name);
            }
        } else {
            themeMsgList = queryById(tagPreName, tagName, DEFAULT_PATH,
                    DEFAULT_NAME);
            themeTempPath = DEFAULT_PATH;
        }
        if (themeMsgList != null && themeMsgList.size() > 0) {
            return themeMsgList;
        } else {
            return null;
        }
    }

    /**
     * 把选择的主题 写到 choose_theme.xml 文件中
     */
    public static boolean writeThemes(String filePath, String fileName,
            String themePath, String whichThemes) {
        File mTempFile = new File(filePath);
        if (!mTempFile.exists()) {
            mTempFile.mkdir();
        }
        mTempFile = new File(filePath + File.separator + fileName);
        if (!mTempFile.exists()) {
            try {
                mTempFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        XmlSerializer serializer = Xml.newSerializer();

        try {
            OutputStream out = new FileOutputStream(filePath + File.separator
                    + fileName);
            out.write("".getBytes());
            out.flush();
            serializer.setOutput(out, "UTF-8");
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "themes");
            serializer.startTag("", "theme");
            serializer.startTag("", "info");
            serializer.attribute("", "id", "whictheme");
            serializer.startTag("", "infos");

            serializer.startTag("", "file_path");
            serializer.text(themePath);
            serializer.endTag("", "file_path");

            serializer.startTag("", "file_name");
            serializer.text(whichThemes);
            serializer.endTag("", "file_name");

            serializer.endTag("", "infos");
            serializer.endTag("", "info");

            serializer.endTag("", "theme");
            serializer.endTag("", "themes");
            serializer.endDocument();
            out.flush();
            out.close();

            return true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

            return false;
        } catch (IllegalStateException e) {
            e.printStackTrace();

            return false;
        } catch (IOException e) {
            e.printStackTrace();

            return false;
        }

    }

    public static int getThemePosition() {
        getSDPath();
        // 读取当前的主题
        ArrayList<HashMap<String, String>> listCurrent = ThemeUtils
                .queryById(THEME_TAG_NAME, WHICH_THEME, WHICH_THEME_PATH,
                        WHICH_THEME_NAME);

        File mTempFile = new File(WHICH_THEME_PATH);
        if (!mTempFile.exists()) {
            mTempFile.mkdir();
        }
        mTempFile = new File(WHICH_THEME_PATH + File.separator
                + WHICH_THEME_NAME);
        if (!mTempFile.exists()) {
            try {
                mTempFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String themeTempPath = null;
        if (listCurrent != null && listCurrent.size() == 2
                && !listCurrent.get(0).get("file_path").equals("")) {
            String file_path = listCurrent.get(0).get("file_path");
            themeTempPath = file_path;
            if (themeTempPath != null) {
                String tempString = themeTempPath.substring(
                        themeTempPath.length() - 2, themeTempPath.length());
                return Integer.parseInt(tempString) - 1;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * 格式化 xml
     * 
     * @param filename
     * @return
     */
    public static int formatXMLFile(String filename) {
        int returnValue = 0;
        try {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(new File(filename));
            XMLWriter output = null;

            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setEncoding("utf-8");
            output = new XMLWriter(new FileWriter(new File(filename)), format);
            output.write(document);
            output.close();

            returnValue = 1;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return returnValue;
    }

    /**
     * @param: headTag 一级标签
     * @param: secondTag 二级标签
     * @param: filePath xml 的文件路径
     * @param: fileName xml的文件名
     * @return: 返回该 二级标签的具体信息列表
     */
    public static ArrayList<HashMap<String, String>> queryById(String headTag,
            String secondTag, String filePath, String fileName) {
        ArrayList<HashMap<String, String>> listMap = null;
        FileInputStream inThemeStream = null;
        boolean isEndTag = false;
        try {
            File tempFile = new File(filePath + File.separator + fileName);
            if (tempFile.exists()) {
                inThemeStream = new FileInputStream(tempFile);
            } else {
                return listMap;
            }
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inThemeStream, null);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                switch (eventType) {
                // 判断当前事件是否为文档开始事件
                case XmlPullParser.START_DOCUMENT:
                    break;
                // 判断当前事件是否为标签元素开始事件
                case XmlPullParser.START_TAG:
                    String tagName = parser.getName();
                    if (tagName.equals(headTag)) {
                        int infoType = parser.getEventType();
                        while (infoType != XmlPullParser.END_DOCUMENT) {
                            switch (infoType) {
                            case XmlPullParser.START_TAG:
                                String infoName = parser.getName();
                                if (infoName.equals("info")) {
                                    listMap = new ArrayList<HashMap<String, String>>();
                                    String attributeNameValue0 = parser
                                            .getAttributeValue(0);

                                    if (secondTag.equals(attributeNameValue0)) {
                                        // 把内容提取到 arrarlist 中
                                        parser.nextTag();
                                        parser.require(XmlPullParser.START_TAG,
                                                null, "infos");

                                        while (parser.nextTag() == XmlPullParser.START_TAG) {
                                            String name = parser.getName();

                                            parser.require(
                                                    XmlPullParser.START_TAG,
                                                    ns, name);
                                            String textValue = readText(parser);
                                            parser.require(
                                                    XmlPullParser.END_TAG, ns,
                                                    name);

                                            HashMap<String, String> tempMap = new HashMap<String, String>();

                                            tempMap.put(name, textValue);
                                            listMap.add(tempMap);
                                        }

                                        parser.require(XmlPullParser.END_TAG,
                                                null, "infos");
                                        isEndTag = true;
                                        break;
                                    } else {
                                        infoType = parser.next();
                                    }
                                } else {
                                    infoType = parser.next();
                                }
                                break;
                            case XmlPullParser.END_TAG:
                                break;
                            default:
                                break;
                            }
                            infoType = parser.next();
                            if (isEndTag)
                                break;
                        }
                    } else {
                    }
                    break;
                // 判断当前事件是否为标签元素结束事件
                case XmlPullParser.END_TAG:
                    break;
                }
                // 进入下一个元素并触发相应事件
                eventType = parser.next();
                if (isEndTag)
                    break;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (inThemeStream != null) {
                inThemeStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listMap;
    }

    public static String readText(XmlPullParser parser) throws IOException,
            XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

}
